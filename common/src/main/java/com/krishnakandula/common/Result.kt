package com.krishnakandula.common

import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.HttpException
import java.io.IOException

sealed class Result<out T> {
    data class Success<out T>(val data: T) : Result<T>()
    data class Error<out T>(val error: Throwable) : Result<T>()
}

fun <T> Observable<T>.retrofitResponseToResult(): Observable<Result<T>> = map { it.asResult() }.onErrorReturn { onError(it) }

fun <T> Single<T>.retrofitResponseToResult(): Single<Result<T>> = map { it.asResult() }.onErrorReturn { onError(it) }

fun <T> Maybe<T>.retrofitResponseToResult(): Maybe<Result<T>> = map { it.asResult() }.onErrorReturn { onError(it) }

fun <T> Flowable<T>.retrofitResponseToResult(): Flowable<Result<T>> = map { it.asResult() }.onErrorReturn { onError(it) }

fun <T> T.asResult(): Result<T> = Result.Success(this)

fun <T> Throwable.asErrorResult(): Result<T> = Result.Error(this)

private fun <T> onError(error: Throwable): Result<T> {
    if (error is HttpException || error is IOException) return error.asErrorResult()
    else throw error
}
