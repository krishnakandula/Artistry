package com.krishnakandula.common

import java.text.SimpleDateFormat
import java.util.*

const val ARTSY_DATE_FORMAT = "YYYY-MM-dd"

fun String.toDate(dateFormat: String): Date = SimpleDateFormat().parse(this)
