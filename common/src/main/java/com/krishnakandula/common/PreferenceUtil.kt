package com.krishnakandula.common

import android.content.Context
import android.preference.PreferenceManager

class PreferenceUtil(private val context: Context) {

    private val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    private val preferenceEditor = sharedPreferences.edit()

    fun putString(key: String, value: String) {
        preferenceEditor.putString(key, value)
    }

    fun getString(key: String): String? = sharedPreferences.getString(key, null)

}
