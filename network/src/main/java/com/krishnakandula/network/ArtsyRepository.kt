package com.krishnakandula.network

import com.krishnakandula.common.Result
import com.krishnakandula.network.service.AuthToken
import io.reactivex.Single

interface ArtsyRepository {

    fun isAuthenticated(): Boolean

    fun getAppToken(clientId: String, clientSecret: String): Single<out Result<AuthToken>>

}
