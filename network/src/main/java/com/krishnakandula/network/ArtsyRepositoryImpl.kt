package com.krishnakandula.network

import com.krishnakandula.common.ARTSY_DATE_FORMAT
import com.krishnakandula.common.PreferenceUtil
import com.krishnakandula.common.Result
import com.krishnakandula.common.toDate
import com.krishnakandula.network.service.ArtsyService
import com.krishnakandula.network.service.AuthToken
import io.reactivex.Single
import java.util.*

class ArtsyRepositoryImpl(private val artsyService: ArtsyService,
                          private val preferenceUtil: PreferenceUtil) : ArtsyRepository {

    private var appAuthToken: String? = null
    private var authTokenExpiryDate: Date = Date()

    private companion object {
        const val AUTH_TOKEN_EXPIRY_DATE = "AUTH_TOKEN_EXPIRY_DATE"
    }

    init {
        val date = preferenceUtil.getString(AUTH_TOKEN_EXPIRY_DATE)
        if (date != null) authTokenExpiryDate = date.toDate(ARTSY_DATE_FORMAT)
    }

    override fun getAppToken(clientId: String, clientSecret: String): Single<Result<out AuthToken>> {
        return artsyService.getAuthToken(clientId, clientSecret)
                .doOnSuccess {
                    when (it) {
                        is Result.Success -> {
                            appAuthToken = it.data.token
                            authTokenExpiryDate = it.data.expiresAt.toDate(ARTSY_DATE_FORMAT)
                            preferenceUtil.putString(AUTH_TOKEN_EXPIRY_DATE, it.data.token)
                        }
                    }
                }
    }

    override fun isAuthenticated(): Boolean = appAuthToken != null && authTokenExpiryDate.after(Date())
}
