package com.krishnakandula.network.service

import com.krishnakandula.common.Result
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ArtsyService {

    @GET("api/tokens/xapp_token")
    fun getAuthToken(@Query("client_id") clientId: String,
                     @Query("client_secret") clientSecret: String): Single<Result<AuthToken>>
}
