package com.krishnakandula.network.service

data class AuthToken(val type: String,
                     val token: String,
                     val expiresAt: String)
